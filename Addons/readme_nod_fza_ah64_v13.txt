----------------------------------
AH-64 PACK BY NODUNIT & FRANZE
PUBLIC RELEASE 09/21/2013 v1.3
----------------------------------

-----------
DISCLAIMER
-----------

If something goes wrong while using this addon, you cannot hold the authors responsible.
Use this package and the included contents at your own risk.
Yes, we really do think it is that awesome.

----------------------------------------------------------------------
For the full online manual, go to:
http://mechmodels.com/mas/downloads/ah64v2/manual/ah64dv2_manual.html
----------------------------------------------------------------------

NOTICE: The contents of this package (textures, models, scripting) were created by
Franze and Nodunit; if you wish to use any of it, please get permission from them
before helping yourself to the content. Thanks!

--------
CREDITS
--------

Nodunit (models, textures)
Franze (scripting)

---Voice---

Bardosy - Kestrel flight
Fangs McCoy - Bitchin' Betty
Franze - Dusk Knight 3-2, Grey Wolf, ground forces
froggyluv - Instructor
nettrucker - Co-Pilot Gunner

--- Additional Credits---

General Barron (getpitch, getbank, setpitch, setbank functions)
Noubernou (clickable cockpits concept, per frame eventhandler)
Robert Hammer (Door open/close sounds, Main Rotor Sounds)
Hellcat (testing, campaign guide)
islefan (testing, advisor)
Jason (testing, advisor)
b00ce (testing)

---Music Credits---

Jeehun Hwang (Heavy Gear, Mechwarrior 2 soundtracks) http://jeehun.com/
Barry Leitch (TFX, Drakan soundtracks) http://www.barryleitch.com/
Origin Skunkworks (Longbow 2 soundtracks)
Bioware (Shattered Steel soundtracks)

---Special Thanks---

Max Power - Testing, model and texture advice
BG ECP - pilot advisor

--------------
VERSION NOTES
--------------

Known issues -
- Player will report 'out of fuel' radio message with CEM on. Reason: to keep
player from being able to take off without going through startup procedure,
fuel is set to 0 until startup procedure has been followed. Cannot disable the
default radio 'out of fuel' message.
- IHADSS toggle sometimes takes several activations/presses to initialize.
- Betty audio messages will play when dead or out of aircraft.
- Audio issues with engine loop sound.
- Sometimes engine startup procedure is skipped.

v1.3 -
- Modified shadow LOD.
- Modified pilot LOD.
- Modified missile guidance.
- Modified missions to account for new damage scripting.
- Modified campaign missions "Pin Drop" and "Hallowed Ground" to include radio
commands for ordering Dusk Knight to stop, continue, or fly to an area. Dusk
Knight will also wait until the player takes off to continue his way route.
- Modified pilot and gunner LODs to have weapons and rotor components.
- Fixed crew placement in crew fire LOD placement that resulted in their being
unhittable except in certain places. Still requires a weapon capable of 
penetrating armor to prevent 'makarov' crew kills.
- Fixed vulnerability in underside of cockpit area.
- Opening doors will now allow crew to be shot.
- Fixed pilot able to use SAL missiles without a gunner.

v1.2 -
- New gun functionality: holding gun fire for 1+ seconds will clear the burst limit,
but in doing so introduces a chance of a jam if gun is overused.
- Changed weapon and ammo class inheritance.
- Modified missile guidance scripting.
- Fixed cockpit glass invulnerability.
- Fixed CPG-Pilot multiplayer issue with gun/pylons.
- Fixed shadow issues.
- Fixed skin selection areas.
- Fixed TADS stow for CPG.

v1.1 -
- Engine startup now has sounds and UFD/WCA page feedback for state of components.
- APU and engines will burn fuel in complex startups.
- IHADSS must be turned on (IHADSS toggle action) after battery turned on.
- Added IHADSS toggle capability to click action on IHADSS storage box.
- UFD, MPDs, and IHADSS in off state until battery, APU, or engines are on.
- Added rotor RPM audio message when exceeding 110% and an altitude above 5m while ENG page active.
- Modified UFD messages to be uniform in color and size; removed some messages.
- Replaced main rotor sounds with Robert Hammer samples.
- Improved shadow LOD.
- M230 burst behavior changed - waiting 1 second or more in the middle of a burst resets burst limit.
- Fixed FCR page components errors with TSD.
- Fixed gunner unable to turn on FCR in single player.
- Fixed Pilot-CPG damage effects - now passed between crew members.
- Fixed click helper not appearing after resuming from a saved game.
- Fixed CPG TSD map not hiding.

v1 -
- Scripting system is unoptimized.
- Binarization causes slowdowns; bone count too high for binarize.
- No LODs for aircraft.
- M230 auto tracking at high angles of bank inaccurate.
- Tail geometry does not disappear after losing tail.
- Aircraft sits partially in the air with missing tail.
- Textures not finished.
- Holes in model when tailboom destroyed. A cap will be made later.
- If two clients are in one aircraft, damage effects are not passed between them.

---------------
INCLUDED FILES
---------------

fza_ah64_us.pbo - Main models and textures file
fza_ah64_controls.pbo - Scripting and controls file
fza_ah64_ctg.pbo - Missions and campaigns file
fza_mi28.pbo - OPFOR Mi-28 file

--------------
INSTALLATION
--------------

All *.pbo files go in your Addons folder. It is recommended to use a mod
folder for all third party addons. Example: @FZA\AddOns\

----------------
QUICK REFERENCE
----------------

FCR On/Off - Salute
FCRAA/FCRAG/HMD/TADS sensor selection - OpticsMode
Cycle right MPD page - Custom User 1
Cycle left MPD page - Custom User 2
WAS/Weapon Action Switch - Custom User 4
Crosshair Action - Custom User 20
IHADSS Mode - Custom User 15
------------------------------------------------------
-----[WPN MUST BE ACTIVE PAGE FOR THE FOLLOWING!]-----
------------------------------------------------------
Gun tracking - Custom User 3
Gun Burst, Rocket Salvo, HELLFIRE trajectory - Binocular
------------------------------------------------------
-----[ASE MUST BE ACTIVE PAGE FOR THE FOLLOWING!]-----
------------------------------------------------------
IR Jammer toggle - Custom User 11
RF Jammer toggle - Custom User 12
Manual/Automatic Jammers - Custom User 13
ASE Autopage - Custom User 14
------------------------------------------------------
---[TSD/FCR MUST BE ACTIVE PAGE FOR THE FOLLOWING!]---
------------------------------------------------------
TSD/FCR target filtering - Custom User 6
TSD Range Setting (IHADSS must be ON!) - Custom User 7
TSD Mode (NAV/ATK) - Custom User 8
PFZ Selection - Custom User 9

------------
USAGE NOTES
------------

- Arming is available by default through the MTVR Ammunition trucks. Arming units can be defined through this array:
fza_ah64_armingunits = ["MtvrReammo_DES_EP1","MtvrReammo"];
When placed in an init field or trigger. When arming your aircraft you must WAIT until the weapons have been loaded,
as indicated by the radio channel field for your own aircraft. Closing the menu prior to the weapons being loaded
will abort the loading process. In multiplayer, a gunner MUST be present to load weaponry with this option. If the
CPG is a player, the CPG MUST be the one to select and equip weaponry.

- Maps can be defined for the TSD map function with these init options:
fza_ah64_mapsizearea = 5120;
fza_ah64_maplist = ["\fza_ah64_US\tex\mpd\notfound_2.paa","\fza_ah64_US\tex\mpd\notfound.paa"];
The first option sets the size of the map in meters; Utes is 5120x5120m, Chernarus is 15360x15360m, and so on. You
will have to determine the size of your map to properly use this option.
The second option sets the map image paths; the images for these maps can be mission based or pbo based. The images
must be ^2 in size and recommended size is no larger than 2048x2048.

- Waypoints are created on the main BIS map; the TSD must be active and the NAV mode must be the current mode.
When ready, use the "Create Waypoints" action to enable the waypoint creation function. When active, click
on the location where you want a waypoint. A green dot will appear on the location and a green circle will appear
at the aircraft's current location. This is Waypoint 0 and is always the aircraft's start waypoint. A maximum of
30 waypoints can be created. Once finished with waypoints, use the "Save Waypoints" action to deactivate the
waypoint creation function and commit the current waypoint list. If you need to clear your waypoints, use the
"Clear Waypoints" action.

- PFZs are created on the main BIS map; to use, first select a PFZ slot (1 - 8) and then use the "Create PFZ"
action. Go to the map and locate your target area on the map, click in one map grid intersection, then click
in another grid intersection. Your PFZ will be created and indicated in white with black letters for the PFZ
number. Any targets in the aircraft's database located in that area will now be added to the indicated PFZ.
The TSD and FCR pages can now use the filtering option for that PFZ; the icons will be indicated in white
when filtered. If not filtered, the targets in the PFZ will be indicated in white amongst all others in green.
Should you so desire, you can link other players to this PFZ with the "PFZ SEND" action in multiplayer.
Doing so allows other players to use the "PFZ RECV" action to download your targets to either their selected
PFZ or their entire database.

- Laser coding is an option for usage with all SAL HELLFIREs. "MODE SELF" returns to default mode of using your
own laser codes and "MODE REMT" cycles through available remote laser codes. "BROADCAST REMOTE CODE" adds your
own laser codes to the global code list and allows you to use a laser designator to designate a target.
"CEASE REMOTE CODE" removes your aircraft from the code list and removes your laser designator. If you are the
firing aircraft, you must first know the direction of the remote laser and your SAL launch mode MUST be an 
LOAL mode.

- If you have a head tracking device, the "Head Tracking On" action allows you to control PNVS and the gun if
the gun tracking mode is HMD/IHADSS. "Head Tracking Off" turns this feature off.

- If equipped with the Longbow FCR, then your aircraft can use the FCR sensor to detect and attack targets.
To use the FCR you must have it selected as a sight and the FCR must be turned on. The FCR takes several
seconds to complete a sweep and process the data, so targets are not always instantly detected. It is also
affected by terrain and distance factors. Once a target is detected it is placed in a central database
and is shown on the TSD page. The FCR page shows only targets detected by the FCR. In addition, the FCR
lacks IFF equipment and makes no distinction between friendly or enemy units.

- The Longbow HELLFIRE missiles (AGM-114L) require the FCR to be on in order to be launched in LOBL mode.
If your aircraft lacks an FCR, the missiles can still be launched in LOAL mode using targeting data from
other aircraft or targeting data already present in the central database. Both the FCR and TADS add target
data to the database. Note that LOBL mode requires a valid LOS to the target to acquire and the FCR
must be on to use LOBL mode.

- The SAL (Laser) HELLFIRE missiles will only work with TADS targeting sensor. They will track the current
TADS target and only one target may be locked at one time. Changing targets while the missile is in flight
will cause the missile to attempt to track the new target. The only exception to this is missiles launched
with a remote laser code. A valid LOS is required for LOBL mode; a valid LOS is only required prior to and
in the terminal phase of a LOAL launch.

- The gun tracking mode cycles between TADS, IHADSS/HMD, auto tracking, and fixed modes. This also affects
the pylon tracking mode. TADS is the default mode, which slaves the gun to the TADS sensor. IHADSS mode
slaves the gun to where the pilot/CPG is currently looking. Autotracking locks onto the current target
or if not present, the current HMD crosshair location. In multiplayer both IHADSS and auto tracking are
linked to the pilots HMD and targets.

- Systems damage is rudimentary at this point but notable are sensors and weapons. Loss of the FCR results
in the FCR remaining inoperable until repaired and thus LOBL AGM-114L mode will no longer operate; TADS
can be rendered inoperable thus rendering self designation of SAL missiles impossible; the gun can either
jam or the actuator can fail, rendering it difficult to use or useless. Further damage indications are
indicative of base systems such as the main transmission, main rotor, tail rotor, etc.

- Incoming missiles can be spoofed by the RF or IR jammers; you have to select the appropriate jammer to jam
the right kind of missile. Most missiles by default are IR, but a couple are RF (notably the SA-19). Use
countermeasures to increase your chances of spoofing a missile but note that IR missiles are not affected
by chaff and RF missiles are not affected by flares.

- The RF jammer can generate false target returns for a short period of time; some units are easier to jam
than others. The jammer is not fool proof since a near miss against a false return could still hit or damage
your aircraft.

- Weapons can be limited in the editor. Here is a list of init field parameters:

fza_ah64_m261pod_qty = 999
fza_ah64_m299rack_qty = 999
fza_ah64_agm114a_qty = 999
fza_ah64_agm114b_qty = 999
fza_ah64_agm114c_qty = 999
fza_ah64_agm114f_qty = 999
fza_ah64_agm114k_qty = 999
fza_ah64_agm114l_qty = 999
fza_ah64_agm114m_qty = 999
fza_ah64_agm114n_qty = 999
fza_ah64_agm114r_qty = 999
fza_ah64_m151_qty = 999
fza_ah64_m229_qty = 999
fza_ah64_m261_qty = 999
fza_ah64_m255_qty = 999
fza_ah64_m257_qty = 999
fza_ah64_m156_qty = 999
fza_ah64_fim92_qty = 999
fza_ah64_30mmhedp_qty = 99999
fza_ah64_auxtank_qty = 999

Quantities are used for every reload, so if a limit of AGM-114L is set at 8 and all 8 are used in the mission,
the missile will no longer be available for loading. Likewise if a weapon is damaged or destroyed, whatever
quantity of munitions that were on that weapon are gone and removed. Some weapons have a minimum required
quantity to load - the M230 requires at least 300 rounds to be able to load, ATAS requires at least 4 missiles
to be equipped. Only Hydras and HELLFIREs will prevent loading of weapons; if the M230 or other weapons
cannot be loaded then they will be passed over and the helicopter will be equipped without them.

- The WAS or Weapon Action Switch function cycles through your available weapons based on type: GUN, RKT, MSL,
ATA, and LSR. WAS will select the first live weapon of the type available, saving you time cycling through
all available weaponry.

- You can perform various functions with the Crosshair Action function. This is bound to Custom User 20 key.

Generic actions:
* IHADSS brightness knob to adjust IHADSS brightness
* screw next to EXT LIGHT toggles interior backlighting
* MPD BRT/Brightness knob adjusts MPD brightness
* Fire panel buttons and test switch perform various effects
* Door handle open/closes door

Right MPD actions:
* TSD and FCR buttons will instantly select those pages
* TSD page:
 * ATK/NAV bezel button
 * MAP bezel button
 * ADD bezel button - in ATK mode enables PFZ creation for selected PFZ; NAV mode enables waypoint
   configuration
 * DEL bezel button - NAV mode clears waypoints
 * RECV bezel button - ATK mode downloads PFZ targets to selected PFZ slot
 * XMIT - ATK mode uploads targets from PFZ; NAV mode saves waypoints
 * RTE - NAV mode binds waypoints to editor set waypoints for pilot's group (requires at least one waypoint)
 * If the waypoint configuration is active, you can also use the Crosshair Action on the TSD viewing area to
   create waypoints on the TSD where the crosshairs are pointing (though this is imprecise).
 * Arrow bezel buttons - adjusts TSD range
 * Filter bezel button - Sets TSD target filter
* ASE page:
 * OPER/STBY/OFF - OPER turns indicated jammer on, STBY sets it to auto on for indicated autopage condition
 * Arrow bezel buttons - adjusts ASE range
 * Autopage bezel button - Autopage setting toggle
* DMS page allows you to hot key to the indicated page on the bezel button

Left MPD actions:
* WPN button sets MPD to WPN page
* WPN page:
 * GUN, MSL, ATA, RKT select respective weapon types
 * BURST settings select equivalent burst settings when GUN selected
 * SELF or REMT in L1 position for SAL missiles
 * TRAJ cycles through missile trajectories
 * QTY in RKT cycles through rocket salvo options
 * ACQ cycles through HMD, AUTO, FXD, or TADS gun tracking modes
 * LRFD turns remote designation on/off
* DMS page allows you to hot key to the indicated page on the bezel button

- FAILURES:
* Losing either display processor will result in only one MPD available. Losing both means losing IHADSS and the MPDs.
Once repaired, MPDs must be reinitialized with the DMS page/button.
* Losing both weapon processors will result in inability to use gun tracking or pylon elevation.
* Rotor damage results in oscillations, rotor failure results in the aircraft crashing.
* Taking hits to the wings and pylons can disable weapons.
* Hits to the gun can jam the gun or disable the actuator.
* Engine fires can be put out with the FIRE panel by arming the respective part and using the primary or reserve fire
bottles. Fires may also be put out by turning off the engines.

-----------
CLASSNAMES
-----------

fza_ah64d_b2e - AH-64D Apache Longbow

fza_ah64d_b2e_nr - AH-64D Apache Longbow No Radar

fza_30x113 - M789 HEDP bullet

fza_agm114l - AGM-114L missile

fza_agm114k - AGM-114K missile

fza_agm114a - AGM-114A missile

fza_agm114c - AGM-114C missile

fza_agm114m - AGM-114M missile

fza_agm114n - AGM-114N missile

fza_275_m151 - M151 rocket

fza_275_m229 - M229 rocket

fza_275_m261 - M261 rocket

fza_275_m255 - M255 rocket

fza_275_m257 - M257 rocket

fza_fim92 - FIM-92 missile

fza_m230 - M230 Chain Gun weapon
Magazines:
fza_m230_1200
fza_m230_350

fza_agm114_23_8 - AGM-114, stations 2 and 3 weapon
Magazines:
fza_agm114l_23_8
fza_agm114k_23_8
fza_agm114c_23_8
fza_agm114a_23_8
fza_agm114m_23_8
fza_agm114n_23_8

fza_agm114_14_8 - AGM-114, stations 1 and 4 weapon
Magazines:
fza_agm114l_14_8
fza_agm114k_14_8
fza_agm114c_14_8
fza_agm114a_14_8
fza_agm114m_14_8
fza_agm114n_14_8

fza_agm114_1_4 - AGM-114, station 1 weapon
Magazines:
fza_agm114l_1_4
fza_agm114k_1_4
fza_agm114c_1_4
fza_agm114a_1_4
fza_agm114m_1_4
fza_agm114n_1_4

fza_agm114_1_ul - AGM-114, station 1 upper left rail weapon
Magazines:
fza_agm114l_1_ul
fza_agm114k_1_ul
fza_agm114c_1_ul
fza_agm114a_1_ul
fza_agm114m_1_ul
fza_agm114n_1_ul

fza_agm114_1_ur - AGM-114, station 1 upper right rail weapon
Magazines:
fza_agm114l_1_ur
fza_agm114k_1_ur
fza_agm114c_1_ur
fza_agm114a_1_ur
fza_agm114m_1_ur
fza_agm114n_1_ur

fza_agm114_1_ll - AGM-114, station 1 lower left rail weapon
Magazines:
fza_agm114l_1_ll
fza_agm114k_1_ll
fza_agm114c_1_ll
fza_agm114a_1_ll
fza_agm114m_1_ll
fza_agm114n_1_ll

fza_agm114_1_lr - AGM-114, station 1 lower right rail weapon
Magazines:
fza_agm114l_1_lr
fza_agm114k_1_lr
fza_agm114c_1_lr
fza_agm114a_1_lr
fza_agm114m_1_lr
fza_agm114n_1_lr

fza_agm114_2_4 - AGM-114, station 2 weapon
Magazines:
fza_agm114l_2_4
fza_agm114k_2_4
fza_agm114c_2_4
fza_agm114a_2_4
fza_agm114m_2_4
fza_agm114n_2_4

fza_agm114_2_ul - AGM-114, station 2 upper left rail weapon
Magazines:
fza_agm114l_2_ul
fza_agm114k_2_ul
fza_agm114c_2_ul
fza_agm114a_2_ul
fza_agm114m_2_ul
fza_agm114n_2_ul

fza_agm114_2_ur - AGM-114, station 2 upper right rail weapon
Magazines:
fza_agm114l_2_ur
fza_agm114k_2_ur
fza_agm114c_2_ur
fza_agm114a_2_ur
fza_agm114m_2_ur
fza_agm114n_2_ur

fza_agm114_2_ll - AGM-114, station 2 lower left rail weapon
Magazines:
fza_agm114l_2_ll
fza_agm114k_2_ll
fza_agm114c_2_ll
fza_agm114a_2_ll
fza_agm114m_2_ll
fza_agm114n_2_ll

fza_agm114_2_lr - AGM-114, station 2 lower right rail weapon
Magazines:
fza_agm114l_2_lr
fza_agm114k_2_lr
fza_agm114c_2_lr
fza_agm114a_2_lr
fza_agm114m_2_lr
fza_agm114n_2_lr

fza_agm114_3_4 - AGM-114, station 3 weapon
Magazines:
fza_agm114l_3_4
fza_agm114k_3_4
fza_agm114c_3_4
fza_agm114a_3_4
fza_agm114m_3_4
fza_agm114n_3_4

fza_agm114_3_ul - AGM-114, station 3 upper left rail weapon
Magazines:
fza_agm114l_3_ul
fza_agm114k_3_ul
fza_agm114c_3_ul
fza_agm114a_3_ul
fza_agm114m_3_ul
fza_agm114n_3_ul

fza_agm114_3_ur - AGM-114, station 3 upper right rail weapon
Magazines:
fza_agm114l_3_ur
fza_agm114k_3_ur
fza_agm114c_3_ur
fza_agm114a_3_ur
fza_agm114m_3_ur
fza_agm114n_3_ur

fza_agm114_3_ll - AGM-114, station 3 lower left rail weapon
Magazines:
fza_agm114l_3_ll
fza_agm114k_3_ll
fza_agm114c_3_ll
fza_agm114a_3_ll
fza_agm114m_3_ll
fza_agm114n_3_ll

fza_agm114_3_lr - AGM-114, station 3 lower right rail weapon
Magazines:
fza_agm114l_3_lr
fza_agm114k_3_lr
fza_agm114c_3_lr
fza_agm114a_3_lr
fza_agm114m_3_lr
fza_agm114n_3_lr

fza_agm114_4_4 - AGM-114, station 4 weapon
Magazines:
fza_agm114l_4_4
fza_agm114k_4_4
fza_agm114c_4_4
fza_agm114a_4_4
fza_agm114m_4_4
fza_agm114n_4_4

fza_agm114_4_ul - AGM-114, station 4 upper left rail weapon
Magazines:
fza_agm114l_4_ul
fza_agm114k_4_ul
fza_agm114c_4_ul
fza_agm114a_4_ul
fza_agm114m_4_ul
fza_agm114n_4_ul

fza_agm114_4_ur - AGM-114, station 4 upper right rail weapon
Magazines:
fza_agm114l_4_ur
fza_agm114k_4_ur
fza_agm114c_4_ur
fza_agm114a_4_ur
fza_agm114m_4_ur
fza_agm114n_4_ur

fza_agm114_4_ll - AGM-114, station 4 lower left rail weapon
Magazines:
fza_agm114l_4_ll
fza_agm114k_4_ll
fza_agm114c_4_ll
fza_agm114a_4_ll
fza_agm114m_4_ll
fza_agm114n_4_ll

fza_agm114_4_lr - AGM-114, station 4 lower right rail weapon
Magazines:
fza_agm114l_4_lr
fza_agm114k_4_lr
fza_agm114c_4_lr
fza_agm114a_4_lr
fza_agm114m_4_lr
fza_agm114n_4_lr

fza_m261_14 - M261 rocket pod, stations 1 and 4 weapon
Magazines:
fza_m261_m151_14_38
fza_m261_m229_14_38
fza_m261_m261_14_38
fza_m261_m255_14_38
fza_m261_m257_14_38

fza_m261_23 - M261 rocket pod, stations 2 and 3 weapon
Magazines:
fza_m261_m151_23_38
fza_m261_m229_23_38
fza_m261_m261_23_38
fza_m261_m255_23_38
fza_m261_m257_23_38

fza_m261_1 - M261 rocket pod, station 1 weapon
Magazines:
fza_m261_m151_1_19
fza_m261_m229_1_19
fza_m261_m261_1_19
fza_m261_m255_1_19
fza_m261_m257_1_19

fza_m261_1_zone1 - M261 rocket pod, station 1 Zone A weapon
Magazines:
fza_m261_m151_1_zone1_12
fza_m261_m229_1_zone1_12
fza_m261_m261_1_zone1_12
fza_m261_m255_1_zone1_12
fza_m261_m257_1_zone1_12

fza_m261_1_zone2 - M261 rocket pod, station 1 Zone B weapon
Magazines:
fza_m261_m151_1_zone2_4
fza_m261_m229_1_zone2_4
fza_m261_m261_1_zone2_4
fza_m261_m255_1_zone2_4
fza_m261_m257_1_zone2_4

fza_m261_1_zone3 - M261 rocket pod, station 1 Zone E weapon
Magazines:
fza_m261_m151_1_zone3_3
fza_m261_m229_1_zone3_3
fza_m261_m261_1_zone3_3
fza_m261_m255_1_zone3_3
fza_m261_m257_1_zone3_3

fza_m261_2 - M261 rocket pod, station 2 weapon
Magazines:
fza_m261_m151_2_19
fza_m261_m229_2_19
fza_m261_m261_2_19
fza_m261_m255_2_19
fza_m261_m257_2_19

fza_m261_2_zone1 - M261 rocket pod, station 2 Zone C weapon
Magazines:
fza_m261_m151_2_zone1_12
fza_m261_m229_2_zone1_12
fza_m261_m261_2_zone1_12
fza_m261_m255_2_zone1_12
fza_m261_m257_2_zone1_12

fza_m261_2_zone2 - M261 rocket pod, station 2 Zone D weapon
Magazines:
fza_m261_m151_2_zone2_4
fza_m261_m229_2_zone2_4
fza_m261_m261_2_zone2_4
fza_m261_m255_2_zone2_4
fza_m261_m257_2_zone2_4

fza_m261_2_zone3 - M261 rocket pod, station 2 Zone E weapon
Magazines:
fza_m261_m151_2_zone3_3
fza_m261_m229_2_zone3_3
fza_m261_m261_2_zone3_3
fza_m261_m255_2_zone3_3
fza_m261_m257_2_zone3_3

fza_m261_3 - M261 rocket pod, station 3 weapon
Magazines:
fza_m261_m151_3_19
fza_m261_m229_3_19
fza_m261_m261_3_19
fza_m261_m255_3_19
fza_m261_m257_3_19

fza_m261_3_zone1 - M261 rocket pod, station 3 Zone C weapon
Magazines:
fza_m261_m151_3_zone1_12
fza_m261_m229_3_zone1_12
fza_m261_m261_3_zone1_12
fza_m261_m255_3_zone1_12
fza_m261_m257_3_zone1_12

fza_m261_3_zone2 - M261 rocket pod, station 3 Zone D weapon
Magazines:
fza_m261_m151_3_zone2_4
fza_m261_m229_3_zone2_4
fza_m261_m261_3_zone2_4
fza_m261_m255_3_zone2_4
fza_m261_m257_3_zone2_4

fza_m261_3_zone3 - M261 rocket pod, station 3 Zone E weapon
Magazines:
fza_m261_m151_3_zone3_3
fza_m261_m229_3_zone3_3
fza_m261_m261_3_zone3_3
fza_m261_m255_3_zone3_3
fza_m261_m257_3_zone3_3

fza_m261_4 - M261 rocket pod, station 4 weapon
Magazines:
fza_m261_m151_4_19
fza_m261_m229_4_19
fza_m261_m261_4_19
fza_m261_m255_4_19
fza_m261_m257_4_19

fza_m261_4_zone1 - M261 rocket pod, station 4 Zone A weapon
Magazines:
fza_m261_m151_4_zone1_12
fza_m261_m229_4_zone1_12
fza_m261_m261_4_zone1_12
fza_m261_m255_4_zone1_12
fza_m261_m257_4_zone1_12

fza_m261_4_zone2 - M261 rocket pod, station 4 Zone B weapon
Magazines:
fza_m261_m151_4_zone2_4
fza_m261_m229_4_zone2_4
fza_m261_m261_4_zone2_4
fza_m261_m255_4_zone2_4
fza_m261_m257_4_zone2_4

fza_m261_4_zone3 - M261 rocket pod, station 4 Zone E weapon
Magazines:
fza_m261_m151_4_zone3_3
fza_m261_m229_4_zone3_3
fza_m261_m261_4_zone3_3
fza_m261_m255_4_zone3_3
fza_m261_m257_4_zone3_3

fza_m261_14_zoneA - M261 rocket pod, stations 1 and 4 Zone A weapon
Magazines:
fza_m261_m151_14_zoneA
fza_m261_m229_14_zoneA
fza_m261_m261_14_zoneA
fza_m261_m255_14_zoneA
fza_m261_m257_14_zoneA

fza_m261_14_zoneB - M261 rocket pod, stations 1 and 4 Zone B weapon
Magazines:
fza_m261_m151_14_zoneB
fza_m261_m229_14_zoneB
fza_m261_m261_14_zoneB
fza_m261_m255_14_zoneB
fza_m261_m257_14_zoneB

fza_m261_14_zoneE - M261 rocket pod, stations 1 and 4 Zone E weapon
Magazines:
fza_m261_m151_14_zoneE
fza_m261_m229_14_zoneE
fza_m261_m261_14_zoneE
fza_m261_m255_14_zoneE
fza_m261_m257_14_zoneE

fza_m261_23_zoneC - M261 rocket pod, stations 2 and 3 Zone C weapon
Magazines:
fza_m261_m151_23_zoneC
fza_m261_m229_23_zoneC
fza_m261_m261_23_zoneC
fza_m261_m255_23_zoneC
fza_m261_m257_23_zoneC

fza_m261_23_zoneD - M261 rocket pod, stations 2 and 3 Zone D weapon
Magazines:
fza_m261_m151_23_zoneD
fza_m261_m229_23_zoneD
fza_m261_m261_23_zoneD
fza_m261_m255_23_zoneD
fza_m261_m257_23_zoneD

fza_m261_23_zoneE - M261 rocket pod, stations 2 and 3 Zone E weapon
Magazines:
fza_m261_m151_23_zoneE
fza_m261_m229_23_zoneE
fza_m261_m261_23_zoneE
fza_m261_m255_23_zoneE
fza_m261_m257_23_zoneE

fza_m261_1234_zoneE - M261 rocket pod, stations 1, 2, 3, and 4 Zone E weapon
Magazines:
fza_m261_m151_1234_zoneE
fza_m261_m229_1234_zoneE
fza_m261_m261_1234_zoneE
fza_m261_m255_1234_zoneE
fza_m261_m257_1234_zoneE

fza_atas_2 - FIM-92/ATAS weapon
Magazines:
fza_atas_2

fza_iafs_100 - Internal Aux tank, 100 gallon magazine

fza_iafs_130 - Internal Aux tank, 130 gallon magazine

fza_auxtank_230_1 - External aux tank, station 1 magazine

fza_auxtank_230_2 - External aux tank, station 2 magazine

fza_auxtank_230_3 - External aux tank, station 3 magazine

fza_auxtank_230_4 - External aux tank, station 4 magazine

fza_usaav - Faction class, "FZA - US Army Aviation"

fza_helicopters - Vehicle class, "FZA - Helicopters"

fza_mi28a - Basic Mi-28A, in package "fza_mi28.pbo"

fza_mi28n - Basic Mi-28N, in package "fza_mi28.pbo"

fza_2a42 - 2A42 cannon for Mi-28, in package "fza_mi28.pbo"
Magazines:
fza_2a42_250_ap
250Rnd_30mmHE_2A42

fza_ataka_16 - Ataka missiles for Mi-28, in package "fza_mi28.pbo"
Magazines:
fza_ataka_16

---------
SWITCHES
---------
fza_ah64_noinit = true - turns off master init
fza_ah64_nofcr = true - turns off FCR sensor
fza_ah64_nofuel = true - turns off fuel page
fza_ah64_noase = true - turns off ASE page
fza_ah64_noeng = true - turns off ENG page
fza_ah64_noflt = true - turns off FLT page
fza_ah64_nowca = true - turns off WCA page
fza_ah64_nowpn = true - turns off WPN page
fza_ah64_notsdfcr = true - turns off TSD/FCR page
fza_ah64_nosave = true - turns off save/reload checker
fza_ah64_noturrets = true - turns turrets off
fza_ah64_noufd = true - turns off UFD
fza_ah64_notargeting = true - turns off IHADSS
fza_ah64_nopfsched = true - turns off per frame scheduler
fza_ah64_norepair = true - disallows quick repair/refuel at arming unit.
fza_ah64_cem = false - turns complex startup off.

fza_ah64_mis_ir - sets which missiles are IR guided for missile spoofing purposes. Default array:
fza_ah64_mis_ir = ["M_R73_AA","M_Strela_AA","M_Igla_AA","M_Stinger_AA","M_Sidewinder_AA","fza_fim92"];

fza_ah64_mis_rf - sets which missiles are RF guided for missile spoofing purposes. Default array:
fza_ah64_mis_rf = ["M_9M311_AA"];

fza_ah64_maplist - sets list of maps for TSD page. Default array:
fza_ah64_maplist = ["\fza_ah64_US\tex\mpd\notfound_2.paa"];

fza_ah64_mapsizearea - sets map size for TSD page. Measured in meters. Default:
fza_ah64_mapsizearea = 5120;

fza_ah64_currentmap - sets default map for TSD page. Default:
fza_ah64_currentmap = "\fza_ah64_US\tex\mpd\notfound_2.paa";

fza_ah64_ada_units - Sets air defense units. Default:
fza_ah64_ada_units = ["ZSU_Base","2S6M_Tunguska","HMMWV_Avenger","M6_EP1"];

fza_ah64_armingunits - Sets arming dialog units. Default:
fza_ah64_armingunits = ["MtvrReammo_DES_EP1","MtvrReammo"];

fza_ah64_m261pod_qty = 999 - M261 rocket pod quantity limit
fza_ah64_m299rack_qty = 999 - M299 rack quantity limit
fza_ah64_agm114a_qty = 999 - AGM-114A quantity limit
fza_ah64_agm114b_qty = 999 - AGM-114B quantity limit
fza_ah64_agm114c_qty = 999 - AGM-114C quantity limit
fza_ah64_agm114f_qty = 999 - AGM-114F quantity limit
fza_ah64_agm114k_qty = 999 - AGM-114K quantity limit
fza_ah64_agm114l_qty = 999 - AGM-114L quantity limit
fza_ah64_agm114m_qty = 999 - AGM-114M quantity limit
fza_ah64_agm114n_qty = 999 - AGM-114N quantity limit
fza_ah64_agm114r_qty = 999 - AGM-114R quantity limit
fza_ah64_m151_qty = 999 - M151 rocket quantity limit
fza_ah64_m229_qty = 999 - M229 rocket quantity limit
fza_ah64_m261_qty = 999 - M261 rocket quantity limit
fza_ah64_m255_qty = 999 - M255 rocket quantity limit
fza_ah64_m257_qty = 999 - M257 rocket quantity limit
fza_ah64_m156_qty = 999 - M156 rocket quantity limit
fza_ah64_fim92_qty = 999 - FIM-92 quantity limit
fza_ah64_30mmhedp_qty = 99999 - 30mm HEDP rounds quantity limit
fza_ah64_auxtank_qty = 999 - Wing auxiliary fuel tank quantity limit